{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}

module RDB.File (readRDB, RDB (..)) where

import Config.Config
import Control.Exception (IOException, catch)
import Control.Monad (void, when)
import Data.Bits (Bits (..))
import Data.ByteString (ByteString)
import qualified Data.ByteString as BS
import Data.ByteString.Char8 (readInt)
import Data.IORef (IORef, modifyIORef, newIORef, readIORef, writeIORef)
import qualified Data.Map as Map
import Data.Text (Text)
import qualified Data.Text as T
import qualified Data.Text.Encoding as TE
import Data.Time (UTCTime, getCurrentTime)
import Data.Time.Clock.POSIX (posixSecondsToUTCTime)
import Data.Word (Word64, Word8)
import GHC.IO.Handle (Handle)
import RESP.Store (StoreValue (..))
import System.IO (IOMode (ReadMode), withBinaryFile)

type KeyVal = (Text, StoreValue)

data RDB
  = RDB {version :: Text, values :: [KeyVal]}
  | InvalidRDB IOException
  deriving (Show)

invalidLengthFormat :: IOError
invalidLengthFormat = userError "invalid length format"

readRDB :: Config -> IO RDB
readRDB config = do
  case filepath config of
    Nothing -> do
      pure . InvalidRDB $ userError "invalid filepath"
    Just file -> do
      withBinaryFile file ReadMode parse `catch` swallowError

swallowError :: IOException -> IO RDB
swallowError e = pure $ InvalidRDB e

invalidRDBError :: IOException -> IO a
invalidRDBError e = do
  putStrLn $ "Invalid RDB: " ++ show e
  ioError e

verifyMagicString :: Handle -> IO ()
verifyMagicString file = do
  redis <- BS.hGet file 5
  when (redis /= "REDIS") $ do
    invalidRDBError $ userError "invalid file format"

verifyVersion :: Handle -> IO Text
verifyVersion file = do
  bVersion <- BS.hGet file 4
  let version = TE.decodeUtf8 bVersion
  pure version

lengthEncoding :: Handle -> IO Int
lengthEncoding file = do
  int8 <- BS.head <$> BS.hGet file 1
  let is00 = not (testBit int8 7) && not (testBit int8 6)
      is10 = testBit int8 7 && not (testBit int8 6)
      is01 = not (testBit int8 7) && testBit int8 6
      is11 = testBit int8 7 && testBit int8 6
      sixLeastSignificatBitsMask = 0x3F
      remainingSixBits = int8 .&. sixLeastSignificatBitsMask
  case () of
    _
      | is00 -> do
          pure $ fromIntegral remainingSixBits
      | is01 -> do
          sndByte' <- BS.head <$> BS.hGet file 1
          let mostSigByte = fromIntegral remainingSixBits `shiftL` 8
              leastSigByte = fromIntegral sndByte'
          pure $ mostSigByte .|. leastSigByte
      | is10 -> do
          maybeInt <- readInt <$> BS.hGet file 4
          case maybeInt of
            Just (int, _) -> pure int
            Nothing -> ioError invalidLengthFormat
      | is11 -> do
          case () of
            _
              -- 0 is 8 bit int
              | remainingSixBits == 0 -> do
                  pure 1
              -- 1 is 16 bit int
              | remainingSixBits == 1 -> do
                  pure 2
              -- 2 is 32 bit int
              | remainingSixBits == 2 -> do
                  pure 4
              -- 3 is compressed string
              | remainingSixBits == 3 -> do
                  lengthEncoding file
              | otherwise -> do
                  let msg = "special format not implemented = " ++ show remainingSixBits
                  ioError $ userError msg
      | otherwise ->
          ioError invalidLengthFormat

verifyAux :: Handle -> IO ()
verifyAux file = do
  klen <- lengthEncoding file
  key <- BS.hGet file klen
  -- reading but ignoring value
  vlen <- lengthEncoding file
  val <- BS.hGet file vlen
  pure ()

verifySelectDB :: Handle -> IO ()
verifySelectDB file = do
  len <- lengthEncoding file
  -- reading but ignoring database number
  BS.hGet file len
  pure ()

resizeDBInfo :: Handle -> IO (Int, Int)
resizeDBInfo file = do
  dbHashTableSize <- lengthEncoding file
  expireHashTableSize <- lengthEncoding file
  pure (dbHashTableSize, expireHashTableSize)

getKeyValue :: Handle -> Word8 -> Maybe UTCTime -> IO KeyVal
getKeyValue file valueType px = do
  case valueType of
    0 -> do
      -- 0 is string
      klen <- lengthEncoding file
      key <- BS.hGet file klen
      vlen <- lengthEncoding file
      val <- BS.hGet file vlen
      let textKey = TE.decodeUtf8 key
          sv = StoreValue {value = TE.decodeUtf8 val, px = px}
      pure (textKey, sv)
    _ -> do
      let msg = "value type not handled = " ++ show valueType
      ioError $ userError msg

handleKeyValue :: Handle -> Word8 -> IORef [KeyVal] -> Maybe UTCTime -> IO ()
handleKeyValue file valueType ref px = do
  kv <- getKeyValue file valueType px
  modifyIORef ref (++ [kv])

handlePX :: Handle -> IORef [KeyVal] -> IO ()
handlePX file ref = do
  unixTimestamp <- BS.hGet file 8
  let pxMillis = eightBytesToWord64 unixTimestamp
      px = posixSecondsToUTCTime $ fromIntegral pxMillis / 1000
  valueType <- BS.head <$> BS.hGet file 1
  now <- getCurrentTime
  if px > now
    then handleKeyValue file valueType ref (Just px)
    else void $ getKeyValue file valueType Nothing -- ignore expired key/val

eightBytesToWord64 :: ByteString -> Word64
eightBytesToWord64 bytes =
  let [b1, b2, b3, b4, b5, b6, b7, b8] = BS.unpack bytes
      word64 =
        (fromIntegral b8 `shiftL` 56)
          .|. (fromIntegral b7 `shiftL` 48)
          .|. (fromIntegral b6 `shiftL` 40)
          .|. (fromIntegral b5 `shiftL` 32)
          .|. (fromIntegral b4 `shiftL` 24)
          .|. (fromIntegral b3 `shiftL` 16)
          .|. (fromIntegral b2 `shiftL` 8)
          .|. fromIntegral b1
   in word64

untilM_ :: (Monad m) => (a -> m Bool) -> (a -> m a) -> a -> m a
untilM_ predicateFn doFn init = do
  done <- predicateFn init
  if done
    then pure init
    else doFn init >>= untilM_ predicateFn doFn

parse :: Handle -> IO RDB
parse file = do
  verifyMagicString file
  version <- verifyVersion file

  eofRef <- newIORef False
  kvRef <- newIORef []
  let isEOF = const $ readIORef eofRef
      handleOpCode = const $ do
        opCode <- BS.head <$> BS.hGet file 1
        case opCode of
          -- AUX
          0xFA -> verifyAux file
          -- SELECTDB
          0xFE -> verifySelectDB file
          -- RESIZEDB
          0xFB -> void $ resizeDBInfo file -- what to do with sizes returned here?
          -- EXPIRETIME
          0xFD -> ioError $ userError "EXPIRETIME not implemented"
          -- EXPIRETIMEMS
          0xFC -> handlePX file kvRef
          -- EOF
          0xFF -> writeIORef eofRef True
          valueType -> handleKeyValue file valueType kvRef Nothing
  untilM_ isEOF handleOpCode ()

  values <- readIORef kvRef
  pure $ RDB {..}
