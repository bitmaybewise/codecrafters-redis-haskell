{-# LANGUAGE OverloadedStrings #-}

module Replica.Handshake (run) where

import Config.Config
import Data.Maybe (fromMaybe)
import Data.Text (Text)
import qualified Data.Text as T
import Network.Simple.TCP (Socket, connect, recv, send)
import RESP.Encoding (toByteString)
import RESP.Spec

run :: ReplicaOf -> Text -> IO ()
run replica@(Replica host port) localPort = do
  connect (T.unpack host) (T.unpack port) $ \(socket, _addr) -> do
    ping socket
    replConf localPort socket
    psync socket

ping :: Socket -> IO ()
ping socket = do
  send socket $ toByteString pingMessage
  validateResponse socket $ RESPSimpleString "PONG"

pingMessage :: Response
pingMessage = RESPArray [RESPBulkString "PING"]

replConf :: Text -> Socket -> IO ()
replConf port socket = do
  send socket $ toByteString (replConfListeningPort port)
  validateResponse socket $ RESPSimpleString "OK"
  send socket $ toByteString replConfCapabilities
  validateResponse socket $ RESPSimpleString "OK"

validateResponse :: Socket -> Response -> IO ()
validateResponse socket respBytes = do
  resp <- recv socket 24
  let expectedResponse = toByteString respBytes
      expectedMessage exp err = "invalid response, expected=\"" ++ show exp ++ "\", got=" ++ show err
      errorWith err = ioError . userError $ expectedMessage expectedResponse err
  case resp of
    Just value -> do
      if value == expectedResponse
        then pure ()
        else errorWith value
    invalid ->
      errorWith $ fromMaybe "EMPTY RESPONSE" invalid

replConfListeningPort :: Text -> Response
replConfListeningPort port =
  RESPArray
    [ RESPBulkString "REPLCONF",
      RESPBulkString "listening-port",
      RESPBulkString port
    ]

replConfCapabilities :: Response
replConfCapabilities =
  RESPArray
    [ RESPBulkString "REPLCONF",
      RESPBulkString "capa",
      RESPBulkString "psync2"
    ]

psync :: Socket -> IO ()
psync socket = do
  send socket $ toByteString psyncMessage
  resp <- recv socket 24 -- ignore response for now
  print resp

psyncMessage :: Response
psyncMessage =
  RESPArray
    [ RESPBulkString "PSYNC",
      RESPBulkString "?", -- Since this is the first time the replica is connecting to the master, the replication ID will be ? (a question mark)
      RESPBulkString "-1" -- Since this is the first time the replica is connecting to the master, the offset will be -1
    ]