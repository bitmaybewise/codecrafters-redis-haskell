{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE OverloadedStrings #-}
{-# OPTIONS_GHC -Wno-unused-top-binds #-}

module Main (main) where

import Config.Config
import Control.Concurrent.STM (atomically)
import Data.ByteString.Char8 (unpack)
import qualified Data.IntMap as Map
import Data.Maybe (fromMaybe)
import qualified Data.Text as TE
import qualified Data.Text.Encoding as T
import Network.Simple.TCP (HostPreference (HostAny), Socket, closeSock, recv, send, serve)
import RDB.File (RDB (..))
import qualified RDB.File as RDB
import RESP.Encoding
import qualified RESP.Spec as RESP
import qualified RESP.Store as Store
import qualified Replica.Handshake as Handshake
import System.Environment (getArgs)

main :: IO ()
main = do
  -- You can use print statements as follows for debugging, they'll be visible when running tests.
  putStrLn "Logs from your program will appear here"

  config <- getArgs >>= parseArgs

  store <- Store.init
  rdb <- RDB.readRDB config
  case rdb of
    InvalidRDB e -> print e
    RDB {} -> Store.load (values rdb) store

  let defaultPort = "6379"
      localPort = TE.unpack $ ("port" `orDefault` config) defaultPort
      replica = replicaOf config
      listeningOn = "Redis server listening on port " ++ localPort
  case replica of
    Master ->
      putStrLn listeningOn
    Replica host port -> do
      putStrLn $ listeningOn ++ ", replica of " ++ TE.unpack host ++ " " ++ TE.unpack port
      Handshake.run replica (TE.pack localPort)

  serve HostAny localPort $ \(socket, address) -> do
    putStrLn $ "successfully connected client: " ++ show address
    evalLoop socket config store
    closeSock socket

evalLoop :: Socket -> Config -> Store.Store k v -> IO ()
evalLoop socket config store = do
  maybeMsg <- recv socket 1024
  case maybeMsg of
    Nothing -> pure ()
    Just msg -> do
      let cmd = RESP.parse $ T.decodeUtf8 msg
      resp <- Store.handle config store cmd
      send socket $ toByteString resp
      evalLoop socket config store
