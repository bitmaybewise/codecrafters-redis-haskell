{-# LANGUAGE OverloadedRecordDot #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}

module RESP.Store where

import Config.Config
import Control.Concurrent (readMVar)
import Control.Concurrent.STM (STM, TMVar, atomically, newTMVarIO, putTMVar, readTMVar, swapTMVar, takeTMVar)
import qualified Data.Map as Map
import Data.Maybe (fromMaybe)
import Data.Text (Text, toLower)
import Data.Time (NominalDiffTime)
import Data.Time.Clock
  ( NominalDiffTime,
    UTCTime,
    addUTCTime,
    getCurrentTime,
  )
import RESP.Spec

data StoreValue = StoreValue {value :: Text, px :: Maybe UTCTime}
  deriving (Show, Eq)

type Store k v = TMVar (Map.Map Text StoreValue)

init :: IO (Store k v)
init = newTMVarIO Map.empty

load :: [(Text, StoreValue)] -> Store k v -> IO ()
load kv mStore = do
  store <- atomically $ takeTMVar mStore
  let rdbValues = Map.fromList kv
      newStore = Map.union store rdbValues
  atomically $ putTMVar mStore newStore

-- Map.union store (Map.fromList kv)

handle :: Config -> Store k v -> Command -> IO Response
handle config store cmd = do
  -- https://redis.io/docs/latest/commands/expire/#how-redis-expires-keys
  now <- getCurrentTime

  case cmd of
    Set {..} -> do
      atomically $ do
        kvStore <- takeTMVar store
        let expiryTime = addMillis now <$> expiry
            storeValue = StoreValue {value = value, px = expiryTime}
            newKVStore = Map.insert (toLower key) storeValue kvStore
        putTMVar store newKVStore
      pure $ RESPSimpleString "OK"
    Get key -> do
      value <- atomically $ do
        kvStore <- readTMVar store
        let maybeValue = Map.lookup (toLower key) kvStore
            expired = fromMaybe False $ do
              st <- maybeValue
              expiryTime <- st.px
              pure $ expiryTime <= now
        if expired
          then do
            let newKVStore = Map.delete (toLower key) kvStore
            kvStore <- swapTMVar store newKVStore
            pure Nothing
          else pure maybeValue
      pure . Value $ do
        sv <- value
        pure $ RESPBulkString sv.value
    Keys _ -> do
      keys <- atomically $ do
        kvStore <- readTMVar store
        pure $ Map.keys kvStore
      pure $ RESPArray (map RESPBulkString keys)
    Echo val ->
      pure $ RESPBulkString val
    Ping ->
      pure $ RESPSimpleString "PONG"
    GetConfig key ->
      case Map.lookup key config of
        Nothing ->
          pure RESPNull
        Just value ->
          pure $ RESPArray [RESPBulkString key, RESPBulkString value]
    Info "replication" ->
      pure . RESPBulkString $ replicationConfig config
    _ ->
      pure UnknownResponse

addMillis :: UTCTime -> Int -> UTCTime
addMillis time int =
  let ms = realToFrac int / 1000 :: NominalDiffTime
   in addUTCTime ms time