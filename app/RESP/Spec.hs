{-# LANGUAGE OverloadedStrings #-}

module RESP.Spec (parse, Command (..), Response (..)) where

import Control.Monad (void)
import Data.Text (Text)
import qualified Data.Text as T
import Data.Void (Void)
import Text.Megaparsec (MonadParsec (try), Parsec, choice, many, optional, runParser, some, (<|>))
import Text.Megaparsec.Char (alphaNumChar, char, crlf, printChar, space1, string, string', symbolChar)
import Text.Megaparsec.Char.Lexer (decimal)

type Parser = Parsec Void Text

data Command
  = Ping
  | Echo Text
  | Set {key :: Text, value :: Text, expiry :: Maybe Int}
  | Get Text
  | Keys [Text]
  | GetConfig Text
  | Info Text
  | UnknownCommand
  deriving (Show)

data Response
  = RESPSimpleString Text
  | RESPBulkString Text
  | RESPArray [Response]
  | RESPNull
  | Value (Maybe Response)
  | UnknownResponse
  deriving (Show)

pArray :: Parser ()
pArray = do
  string "*"
  decimal
  crlf
  pure ()

pCmdString' :: Text -> Parser Text
pCmdString' cmd = do
  string "$"
  decimal
  crlf
  string' cmd
  crlf

pStringVal :: Parser Text
pStringVal = do
  string "$"
  decimal
  crlf
  val <- T.pack <$> some alphaNumChar
  crlf
  pure val

pSymbol :: Char -> Parser Text
pSymbol sym = do
  string "$"
  decimal
  crlf
  val <- T.singleton <$> char sym
  crlf
  pure val

pInt :: Parser Int
pInt = do
  string "$"
  decimal
  crlf
  int <- decimal
  crlf
  pure int

pPing :: Parser Command
pPing = do
  pArray
  pCmdString' "ping"
  pure Ping

pEcho :: Parser Command
pEcho = do
  pArray
  pCmdString' "echo"
  Echo <$> pStringVal

pSetExpiry :: Parser Int
pSetExpiry = do
  pCmdString' "px"
  pInt

pSet :: Parser Command
pSet = do
  pArray
  pCmdString' "set"
  key <- pStringVal
  val <- pStringVal
  expiryTime <- optional pSetExpiry
  pure $ Set {key = key, value = val, expiry = expiryTime}

pGet :: Parser Command
pGet = do
  pArray
  pCmdString' "get"
  Get <$> pStringVal

pKeys :: Parser Command
pKeys = do
  pArray
  pCmdString' "keys"
  keys <- try (many pStringVal) <|> ((: []) <$> pSymbol '*')
  pure $ Keys keys

pGetConfig :: Parser Command
pGetConfig = do
  pArray
  pCmdString' "config"
  pCmdString' "get"
  GetConfig <$> pStringVal

pInfo :: Parser Command
pInfo = do
  pArray
  pCmdString' "info"
  Info <$> pStringVal

pCommand :: Parser Command
pCommand =
  choice
    [ try pPing,
      try pEcho,
      try pSet,
      try pGet,
      try pKeys,
      try pGetConfig,
      try pInfo
    ]

parse :: Text -> Command
parse = do
  result <- runParser pCommand ""
  pure $ case result of
    Left _ -> UnknownCommand
    Right cmd -> cmd
