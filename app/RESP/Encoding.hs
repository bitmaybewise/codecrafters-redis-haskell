{-# LANGUAGE OverloadedStrings #-}

module RESP.Encoding where

import Data.ByteString (ByteString)
import qualified Data.ByteString.Char8 as BS
import Data.List (foldl')
import Data.Maybe (fromMaybe)
import qualified Data.Text as T
import Data.Text.Encoding (encodeUtf8)
import RESP.Spec

toByteString :: Response -> ByteString
toByteString cmd =
  case cmd of
    Value maybeVal ->
      maybe "$-1\r\n" toByteString maybeVal
    RESPNull ->
      "_\r\n"
    RESPSimpleString val ->
      "+" <> encodeUtf8 val <> "\r\n"
    RESPBulkString val ->
      let valLen = BS.pack . show . T.length $ val
       in "$" <> valLen <> "\r\n" <> encodeUtf8 val <> "\r\n"
    RESPArray values ->
      let bulkStrings = foldl' (\acc v -> acc <> toByteString v) "" values
          valLen = BS.pack . show . length $ values
       in "*" <> valLen <> "\r\n" <> bulkStrings
    _ ->
      "-Unknown command\r\n"
