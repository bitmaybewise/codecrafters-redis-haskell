{-# LANGUAGE OverloadedStrings #-}

module Config.Config
  ( parseArgs,
    Config,
    filepath,
    orDefault,
    replicaOf,
    ReplicaOf (..),
    replicationConfig,
  )
where

import Data.List (elemIndex)
import Data.Map (Map)
import qualified Data.Map as Map
import Data.Maybe (fromMaybe)
import Data.Text (Text, pack, splitOn, unpack)

type Config = Map Text Text

data ReplicaOf
  = Master
  | Replica Text Text
  deriving (Show)

parseArgs :: [String] -> IO Config
parseArgs args =
  let nextElem idx =
        if length args > idx
          then Just . pack $ args !! (idx + 1)
          else Nothing
      dir = elemIndex "--dir" args >>= nextElem
      dbfilename = elemIndex "--dbfilename" args >>= nextElem
      port = elemIndex "--port" args >>= nextElem
      replicaof = elemIndex "--replicaof" args >>= nextElem
   in pure
        $ insertIfPresent "dir" dir
          . insertIfPresent "dbfilename" dbfilename
          . insertIfPresent "port" port
          . (isValidReplicaOf *> insertIfPresent "replicaof") replicaof
          . insertReplID
          . insertReplOffset
        $ Map.empty

insertIfPresent :: Text -> Maybe Text -> Config -> Config
insertIfPresent _ Nothing config = config
insertIfPresent key (Just value) config = Map.insert key value config

orDefault :: Text -> Config -> Text -> Text
orDefault key config default' = fromMaybe default' $ Map.lookup key config

filepath :: Config -> Maybe FilePath
filepath cfg = do
  filename <- Map.lookup "dbfilename" cfg
  dir <- Map.lookup "dir" cfg
  pure . unpack $ dir <> "/" <> filename

isValidReplicaOf :: Maybe Text -> Maybe Bool
isValidReplicaOf mValue = do
  replicaof <- mValue
  let splitStr = splitOn " " replicaof
  pure $ length splitStr == 2

replicaOf :: Config -> ReplicaOf
replicaOf cfg = case Map.lookup "replicaof" cfg of
  Nothing -> Master
  Just replicaof ->
    let [host, port] = splitOn " " replicaof
     in Replica host port

replicaRole :: ReplicaOf -> Text
replicaRole Master = "master"
replicaRole (Replica _ _) = "slave"

insertReplID :: Config -> Config
insertReplID = Map.insert "master_replid" "8371b4fb1155b71f4a04d3e1bc3e18c4a990aeeb"

insertReplOffset :: Config -> Config
insertReplOffset = Map.insert "master_repl_offset" "0"

replicationConfig :: Config -> Text
replicationConfig cfg =
  let replKeys =
        insertIfPresent "master_replid" (Map.lookup "master_replid" cfg)
          . insertIfPresent "master_repl_offset" (Map.lookup "master_repl_offset" cfg)
          . insertIfPresent "role" (Just . replicaRole . replicaOf $ cfg)
          $ Map.empty
   in Map.foldlWithKey' (\acc k v -> acc <> "\r\n" <> k <> ":" <> v) "" replKeys
